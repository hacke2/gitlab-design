# GitLab Sketch UI Kit -- DEPRECATED

The Sketch UI Kit has been deprecated in favor of our [Pajamas UI Kit in Figma](https://www.figma.com/file/qEddyqCrI7kPSBjGmwkZzQ/Pajamas-UI-Kit) and will no longer be updated. [Learn how to contribute to our Pajamas UI Kit](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/pajamas-ui-kit.md).
